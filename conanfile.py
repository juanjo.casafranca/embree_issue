from conan import ConanFile
from conan.tools.cmake import CMakeDeps, CMakeToolchain, cmake_layout
from conan.tools.env import VirtualRunEnv


class EmbreeIssue(ConanFile):
    settings = "os", "compiler", "build_type", "arch"

    requires = (
        "embree3/3.13.5",
        "cgal/5.5.1",
    )

    def generate(self):
        cmake = CMakeDeps(self)
        cmake.generate()

        toolchain = CMakeToolchain(self)
        toolchain.variables["CMAKE_MAP_IMPORTED_CONFIG_DEBUG"] = "Release"
        toolchain.generate()

        venv = VirtualRunEnv(self)
        venv.generate()

    def layout(self):
        cmake_layout(self)
